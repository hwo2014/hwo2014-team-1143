import json
import socket
import sys


class NoobBot(object):

    def __init__(self, socket, name, key, accel = 1.0):
        self.socket = socket
        self.name = name
        self.key = key
        
        self.color = ''
        
        #I added this variable so we could change the acceleration
        self.accel = accel
        
        #another variable to keep track of the current game tick
        self.raceTick = 0
        
        #this variable keeps track on if we're crashed
        self.is_crashed = False
        
        #this variable keeps track of if we are in a turn
        self.in_turn = False
        
        #these variable will hold all of the track info
        self.pieces = [dict()]
        self.lanes = [dict()]
        self.cars = [dict()]

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.cars = data
        self.throttle(self.accel)


    def on_crash(self, data):
        print("Someone crashed")
        if self.name == data['name']:
            self.is_crashed = True
        self.ping()
        
    #custom on spawn member function
    def on_spawn(self, data):
        if self.is_crashed and self.name == data['name']:
            self.accel -= .1
            self.throttle(self.accel)
            self.is_crashed = False



    def on_init(self, data):
        self.pieces = data['race']['track']['pieces']
        self.lanes = data['race']['track']['lanes']

    def on_game_end(self, data):
        print("Race ended")
        self.ping()



    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
        
    #This function really just gives our car a color
    def on_yourCar(self, data):
        self.color = data['color']

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'spawn': self.on_spawn,
            'yourCar': self.on_yourCar,
            'gameInit': self.on_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if msg_type == 'carPosition':
                    self.raceTick = msg['gameTick']
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
